# Hypothyroid model (Decision trees / SVM)

### By: Andrew Wairegi

## Description
To create a model that will predict the diagnosis of a patient have hypothyroid disease, 
with an acccuracy of atleast 80%. Using decision trees and support vector machines. This will allow 
the hospital to diagnose patients using statistical data. As opposed to a doctor diagnosing them manually. 
This will allow them to save time, and money. As they will not need to hire a doctor to diagnose a patient.
It can also serve as prediagnosis.

[Open notebook]

## Setup/installation instructions
1. Find a local folder on the computer
2. Set it up as an empty repository (git init)
3. Clone the repository there (into the local folder) (git clone https://...)
4. Upload the notebook to a google drive folder
5. Open it
6. Upload the data files to google collab (in to the file upload section)
7. Run the notebook

## Known Bugs
There are no known issues / bugs

## Technologies Used
1. Python - The programming language
2. Numpy - An Arithmetic Package
3. Pandas - A Data Analysis Package
4. Seaborn - A Visualization package
5. Matplotlib - A Visualization package
6. Scikit learn - A Modelling package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/machine-learning/Decisiontrees_SVM-Hypothyroid_classification/-/blob/main/Hypothyroid_check_(Decision_tree_&_SVM).ipynb
